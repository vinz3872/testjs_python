var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var circlePos = [];
var component = document.getElementById("Component");

function draw_circle(arr) {
	if (arr[2] == 0)
		ctx.strokeStyle = '#ff0000';
	else
		ctx.strokeStyle = '#000000';

	ctx.beginPath();
	ctx.arc(arr[0], arr[1], 75, 0, 2 * Math.PI);
	ctx.stroke();
}

function checkCollapse(arr) {
	var ret = 0;
	var tmp = 0.00;
	for (var i = 0; i < circlePos.length; i++) {
		tmp = Math.sqrt((arr[0]-circlePos[i][0])*(arr[0]-circlePos[i][0]) + (arr[1]-circlePos[i][1])*(arr[1]-circlePos[i][1]));
		if ((arr[0] != circlePos[i][0] || arr[1] != circlePos[i][1])
			&& tmp < 152 && arr[2] != circlePos[i][2]) {
			circlePos[i][2] = 0;
			arr[2] = 0;
			ret = 1;
		}
	}
	return ret;
}

function countComponent() {
	var val = 0;
	for (var i = 0; i < circlePos.length; i++) {
		if (circlePos[i][2] == 1)
			val++;
	}
	val++;
	component.innerHTML = "Number of components: " + val;
}

function redraw(x, y) {
	if (circlePos.length == 0) {
		circlePos.push([x, y, 0]);
		ctx.strokeStyle = '#ff0000';
	} else {
		circlePos.push([x, y, 1]);
		ctx.strokeStyle = '#000000';
	}
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	var ret = 0;
	for (var i = 0; i < circlePos.length; i++) {
		ret = checkCollapse(circlePos[i]);
		if (ret == 1)
			i = -1;
	}
	for (var i = 0; i < circlePos.length; i++) {
		draw_circle(circlePos[i]);
	}
	countComponent();
	console.log(JSON.stringify(circlePos));
}

document.getElementById('myCanvas').onclick = function(e){
	redraw(e.pageX, e.pageY)
}
